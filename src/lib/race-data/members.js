const csv = require('csvtojson')
const path = require('path')

// console.log(`Require MembersDB`)

class MembersDB {
  static async loadMembers() {
    const memberFile = path.join(__dirname, '../../race-data/member.csv')
    return (await csv({ checkType: true }).fromFile(memberFile)).map(
      ({ first, last, sex, dob, member, tag }) => ({
        name: `${first} ${last}`,
        first,
        last,
        gender: sex,
        dob: MembersDB.fixDOB(dob),
        buacMemberId: member,
        buacTag: tag,
      })
    )
  }

  static fixDOB(dob) {
    const fixedString = dob
      .split('.')
      .reverse()
      .join('-')
    const date = new Date(fixedString)
    if (date) {
      return date
    }
    throw new Error(`Could not fix DOB string ${dob}`)
  }

  static getKnownNames(name) {
    return {
      'Mel Debnam': 'Mel Debnam-Smith',
    }
  }

  static async memberByNumber(memberNumber) {
    const filtered = MembersDB.members.filter(
      m => m.buacMemberId === memberNumber
    )
    switch (filtered.length) {
      case 1:
        return filtered[0]
      case 0:
        return null
      default:
        throw new Error(
          `${filtered.length} members have number ${memberNumber}`
        )
    }
  }
  static async memberByName(name) {
    if (!MembersDB.loaded) {
      // TODO cache members
      //console.log(`loading members for ${name}`)
      MembersDB.members = await MembersDB.loadMembers()
      MembersDB.loaded = true
      MembersDB.knownNames = MembersDB.getKnownNames()
    } else {
      //console.log(`Not loading members for ${name} ${MembersDB.members.length}`)
    }
    //console.log({ members })
    const filtered = MembersDB.members.filter(m => m.name === name)
    //console.log('memberByName',{name,filtered,})
    switch (filtered.length) {
      case 1:
        return filtered[0]
      case 0:
        const corrected = MembersDB.knownNames[name]
        if (corrected) {
          return MembersDB.memberByName(corrected)
        }
        return null
      default:
        throw new Error(`${filtered.length} memmbers match ${name}`)
    }
  }
}

MembersDB.loaded = false
MembersDB.members = []
;(async function() {
  //console.log('LOADING MEMBERS FILE')
  MembersDB.members = await MembersDB.loadMembers()
  MembersDB.loaded = true
  MembersDB.knownNames = MembersDB.getKnownNames()
  // console.log('LOADED MEMBERS FILE')
})()

module.exports = MembersDB
// console.log(`DONE require MembersDB`)
