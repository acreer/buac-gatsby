// So we have found a race folder (ATOW by finding a frontMatter.yaml) and now
// we want to return all the data

const fs = require('fs')
const path = require('path')
const csv = require('csvtojson')
const YAML = require('yaml')
const { isString, isNumber, isEqual, flatten } = require('lodash')
const firstline = require('firstline')

const {
  HANDICAP,
  RESULT,
  POINTS,
  POINTS_CLUB,
  POINTS_SPEED,
  OTHER_RESULT,
  LONG,
  MEDIUM,
  SHORT,
  MemberNumberUnknown,
  NewRunner,
} = require('./constants')
const MembersDB = require('./members')
const { toNumber, transform, camelCase } = require('lodash')

// Need to get length from filename. All headers are the same
const modernLengthRegexp = /_(SHORT|MEDIUM|LONG).csv/
const easyLengthKmRegexp = /-(\d[\d\.]*?)km/
const olderLengthRegexp = /\/(short|medium|long)\//i
const fileNameRaceLength = file => {
  if (modernLengthRegexp.exec(file)) {
    // watch regexp and constants....
    // console.log(`modernLengthRegexp ${file}`)
    return RegExp.$1
  }
  const kmLen = file.match(easyLengthKmRegexp)
  if (file.match(easyLengthKmRegexp)) {
    // console.log(`easyLengthKmRegexp ${file}`)
    return `${RegExp.$1} km`
  }
  if (olderLengthRegexp.test(file)) {
    // console.log(`olderLengthRegexp ${file}`)
    return RegExp.$1.toUpperCase()
  }
  // console.log(`No buacLength from file '${file}'`)
  return null
}

const transformFrontmatter = ({ dir }) => {
  const raw = transform(
    YAML.parse(fs.readFileSync(path.join(dir, 'frontMatter.yaml'), 'utf8')),
    (result, value, key) => (result[camelCase(key)] = value)
  )
  raw.date = new Date(raw.date)

  // here we change the names of fields in raw yaml file
  // into stuff that is less ambiguous in the graphQL etc.
  const {
    number: buacRunNumber,
    date: buacRunDate,
    name: buacRunName,
    runMd: buacRunId,
  } = raw

  if (!buacRunId) {
    console.log(`No runMd in ${dir}`)
  }
  const warningRunId = buacRunId || `noRunId${dir}`

  const transformed = {
    buacRunId: warningRunId.replace(/\.md$/, ''),
    buacRunDate,
    buacRunNumber,
    buacRunName,
    buacYear: buacRunDate.getFullYear(),
  }
  // console.log({ transformed })
  return transformed
}
exports.transformFrontmatter = transformFrontmatter

const yearRex = /^\d{4}$/

const csvsGroupBy = (csvs, csvRowKeyFunc = row => `${row.first} ${row.last}`) =>
  csvs.reduce((acc, csvInfo) => {
    csvInfo.csv.forEach(row => {
      const key = csvRowKeyFunc(row)
      if (!acc[key]) acc[key] = []
      acc[key].push(row)
    })
    return acc
  }, {})

exports.onRaceFolder = async ({ dir }) => {
  const filenames = walk(dir)
  const meta = transformFrontmatter({ dir })
  const csvs = await Promise.all(
    filenames
      .filter(fn => fn.match(/\.csv$/))
      .map(async filename => {
        const buacType = csvHeaderToFileType(
          await firstline(filename),
          filename
        )
        const buacTypeExtra = [OTHER_RESULT, POINTS].some(
          type => buacType === type
        )
          ? filenameBuacType(filename)
          : null
        return {
          buacLength: fileNameRaceLength(filename),
          buacType,
          buacTypeExtra,
          csv: await loadNormalisedCSV(filename),
          ...meta,
          filename: filename
            .replace(path.resolve(path.join(__dirname, '../../')), '')
            .replace(/^\//, ''),
        }
      })
  )
  meta.uniqueRunners = Object.keys(
    csvsGroupBy(
      csvs.filter(
        csvInfo =>
          //[RESULT,OTHER_RESULT].some(type=>csvInfo.buacType===type)
          csvInfo.buacType == RESULT || csvInfo.buacType == OTHER_RESULT
      )
    )
  ).length
  return { csvs, meta }
}

// name gets changed to fullname by normalise
const csvHeaderTypeMap = {
  [HANDICAP]: [
    ['estTime', 'first', 'last', 'member', 'pace', 'place', 'start'], //Post15
    ['estTime', 'first', 'last', 'member', 'pace', 'start'], //2014
    ['estTime', 'first', 'last', 'pace', 'place', 'start'], //2014 coll
    ['estTime', 'first', 'last', 'pace', 'start'], //2011
  ],
  [RESULT]: [
    ['diff', 'first', 'last', 'netTime', 'pace', 'place'], //post 2105
    ['diff', 'first', 'last', 'netTime', 'place'], //2015 TODO inject a pace?
    ['first', 'last', 'netTime', 'place'], //2009
  ],
  [POINTS]: [['first', 'last', 'place', 'points', 'races']],
  [OTHER_RESULT]: [
    ['club', 'first', 'last', 'pace', 'place', 'time'], // 2019 Half and invite
    // ['diff', 'guess', 'name', 'net', 'pace', 'place'], // 2019 mystery
    ['diff', 'guess', 'name', 'pace', 'place', 'time'], //
    ['club', 'name', 'pace', 'place', 'time'], //2016 half junior
    ['age', 'club', 'gender', 'name', 'pace', 'place', 'time'], //2016 half senior
    // ['age', 'club', 'gender', 'name', 'pace', 'place', 'time'],
    ['club', 'first', 'last', 'netTime', 'place'], // 2015 invite
    ['club', 'first', 'last', 'netTime', 'pace', 'place'], // 2015 half
    ['club', 'first', 'last', 'place', 'time'],
  ],
}

// Sometimes get buacType from csv header,
const csvHeaderToFileType = (line, filename = 'unknown file') => {
  const fields = line
    .toLowerCase()
    .split(',')
    .map(f => camelCase(f))
    .sort()
  const buacType = Object.keys(csvHeaderTypeMap).find(buacType =>
    csvHeaderTypeMap[buacType].some(choice => isEqual(choice, fields))
  )
  if (!buacType) {
    console.log(
      `csvHeaderToFileType unknown buacType in '${filename.replace(
        process.cwd() + '/',
        ''
      )}'`,
      {
        line,
        fields: fields.sort(),
      }
    )
  }
  return buacType
}

const lookAfterLen = /km-(.*)\.csv$/
// We only call this is we need to differentiate type
// that we cant get from header line.
// ie 21km-men-40+ 21km men-50+
const filenameBuacType = filename => {
  if (filename.match(/adj_/)) {
    return POINTS_CLUB
  }
  if (filename.match(/net_/)) {
    return POINTS_SPEED
  }
  if (filename.match(lookAfterLen)) {
    return RegExp.$1
  }
  return path.basename(filename, '.csv')
  // We only want a value for
}

exports.filenameBuacType = filenameBuacType
exports.fileNameRaceLength = fileNameRaceLength
exports.csvHeaderToFileType = csvHeaderToFileType

function walk(dir) {
  const results = []
  const list = fs.readdirSync(dir)
  list.forEach(function(file) {
    file = dir + '/' + file
    const stat = fs.statSync(file)
    if (stat && stat.isDirectory()) {
      results.push(walk(file))
    } else {
      results.push(file)
    }
  })
  return flatten(results)
}
exports.walk = walk

const loadNormalisedCSV = async fn =>
  Promise.all(
    (await csv({ checkType: true }).fromFile(fn)).map(
      async r => await normaliseCsv(r, fn)
    )
  )

// CamelCase with member
// We ALWAYS put name , we leave first last in present , remove fullname
async function normaliseCsv(record, filename = 'Unknown File') {
  const camelCased = transform(record, (result, value, key) => {
    result[camelCase(key)] = value
  })

  if (isString(camelCased.place) && camelCased.place.toUpperCase() === 'NEW')
    camelCased.place = NewRunner

  // Start looking up member by fullname
  if (!camelCased.name) {
    if (camelCased.first && camelCased.last) {
      camelCased.name = `${camelCased.first} ${camelCased.last}`
    } else if (camelCased.fullname) {
      camelCased.name = camelCased.fullname
      delete camelCased.fullname
    } else {
      if (!camelCased.member) {
        console.log(
          `No name for memberLookup ${filename.replace(
            process.cwd() + '/',
            ''
          )}`
        )
        console.log({ record, camelCased })
      }
      //process.exit()
    }
  }
  let member
  if (camelCased.member) {
    member = await MembersDB.memberByNumber(camelCased.member)
    //console.log({member,number:camelCased.member})
  } else {
    member = await MembersDB.memberByName(camelCased.name)
    //console.log({camelCased,member})
  }
  if (member) {
    camelCased.first = member.first
    camelCased.last = member.last
    camelCased.name = `${camelCased.first} ${camelCased.last}`
    camelCased.member = member.buacMemberId
  } else {
    camelCased.member = MemberNumberUnknown
    // console.log(`No member info in ${filename}`,{camelCased,record})
  }
  // TODO AFter we have a friends of the club file with first and last....
  if (!camelCased.first && !camelCased.last && !camelCased.name) {
    console.log(
      `No names after memberLookup '${filename.replace(
        process.cwd() + '/',
        ''
      )}'`
    )
    console.log({ record, camelCased })
  }
  // force strings
  ;['pace', 'diff', 'time', 'netTime'].forEach(k => {
    const val = camelCased[k]
    if (val && isNumber(val)) {
      // TODO change pace 7.35 into 07:35? OR change to seconds and
      // let front end deal?
      //console.log(`normaliseCsv: Numeric ${k}=${val} in ${filename}`)
      camelCased[k] = val.toString().replace('.', ':')
    }
  })
  // There are some empty fields with no headers
  delete camelCased.field6
  return camelCased
}

exports.normaliseCsv = normaliseCsv
exports.loadNormalisedCSV = loadNormalisedCSV
