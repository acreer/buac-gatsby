const {
  isHandicap,
  fileNameRaceLength,
  filenameBuacType,
  isResult,
  normaliseCsv,
  csvHeaderToFileType,
} = require('./on-race-folder')

const { forEach } = require('lodash')

const {
  SHORT,
  LONG,
  NewRunner,
  MemberNumberUnknown,
  HANDICAP,
  RESULT,
  OTHER_RESULT,
  POINTS,
  POINTS_CLUB,
  POINTS_SPEED,
} = require('./constants')

const MembersDB = require('./members')

describe(`csv header gives information`, () => {
  describe('BuacType', () => {
    forEach(
      {
        [HANDICAP]: [
          'Place,Member,First,Last,Start,Pace,Est Time',
          'Place ,Member ,First,Last,Start,Pace,Est Time',
          'Member ,place ,First,Last,Start,Pace,Est Time',
          'Place,Member,First,Last,Start,Pace,Est Time', //2017
          // 'Place,Tag,First,Last,Start,Pace,Est Time', //2017
        ],
        [RESULT]: [
          'Place,First,Last,NET Time,Pace,Diff',
          'Place , First ,Last, NET Time ,Pace,Diff',
          'Place,First,Last,NET Time,Pace,Diff', //2017
          'Place,First,Last,NET Time,Pace,Diff', //2017
        ],
        [POINTS]: [
          'PLACE,FIRST,LAST,RACES,POINTS',
          'PLACE,FIRST,LAST,RACES,POINTS', //2017
        ],
        [OTHER_RESULT]: [
          ' PLACE, FIRST, LAST, TIME, PACE, CLUB ',
          //' PLACE,NAME,PACE,NET,GUESS,DIFF ',
          'PLACE,FIRST,LAST,TIME,PACE,CLUB', //2017
          //'PLACE,NAME,PACE,NET,GUESS,DIFF', //2017
          //'PLACE,NAME,PACE,NET,GUESS,DIFF', //2017
          'PLACE,NAME,TIME,PACE,CLUB', //2016
          'PLACE,NAME,TIME,PACE,CLUB,GENDER,AGE', //2016
          //'place,firstname,surname,time,club', //2015
        ],
      },
      (examples, csvType) => {
        examples.forEach(header =>
          test(`buacType '${csvType}' from ${header}`, () =>
            expect(csvHeaderToFileType(header)).toEqual(csvType))
        )
      }
    )
  })
})

describe('filenames giving information', () => {
  describe('regular modern club runs', () => {
    describe(`given modernShortHandicap`, () => {
      const modernShortHandicap =
        '2019/01_college-classic/01_College_Classic_handicaps_SHORT.csv'
      test(`SHORT from ${modernShortHandicap}`, () => {
        expect(fileNameRaceLength(modernShortHandicap)).toBe(SHORT)
      })
    })

    describe(`Given a modernLongResult`, () => {
      const modernLongResult =
        '2019/01_college-classic/01_College_Classic_results_LONG.csv'
      test(`LONG: ${modernLongResult}`, () =>
        expect(fileNameRaceLength(modernLongResult)).toBe(LONG))
    })
    describe('Points filenames', () => {
      const speedFile =
        'src/race-data/2019/01_college-classic/net_points_MEDIUM.csv'
      const clubFile =
        'src/race-data/2019/01_college-classic/adj_points_LONG.csv'
      test(`Speed ${speedFile}`, () =>
        expect(filenameBuacType(speedFile)).toBe(POINTS_SPEED))
      test(`Club ${speedFile}`, () =>
        expect(filenameBuacType(clubFile)).toBe(POINTS_CLUB))
    })
  })
  describe('A sorted uni invite', () => {
    const filePath =
      'results/long/02-14/02-ages/2019-Uni-Half-Marathon-14.0km-50plus-Female.csv'
    test(`Length '${filePath}'`, () =>
      expect(fileNameRaceLength(filePath)).toBe('14.0 km'))
  })
  describe('Modern Uni Invites', () => {
    const fn = '2019-Uni-Invite-7.6km-ALL-Female.csv'
    describe('Lengths', () => {
      test(`easy ${fn}`, () => expect(fileNameRaceLength(fn)).toBe('7.6 km'))
    })
    describe('Type', () => {
      test(`type ${fn}`, () => expect(filenameBuacType(fn)).toBe('ALL-Female'))
    })
  })
})

describe('given a 24.5.1971', () => {
  test('is a date', () => {
    const date = MembersDB.fixDOB('24.5.1971')
    expect(date.getFullYear()).toBe(1971)
    expect(date.getMonth()).toBe(4) // zero based months
    expect(date.getDate()).toBe(24)
  })
})

describe('MembersDB', () => {
  test('Andrew Creer', async () => {
    expect(await MembersDB.memberByName('Andrew Creer')).toMatchObject({
      buacMemberId: 33,
      first: 'Andrew',
      last: 'Creer',
    })
  })
  describe('Member names change over time', () => {
    test(`Mel Debnam is '1028,Mel,Debnam-Smith,'`, async () => {
      expect(await MembersDB.memberByName('Mel Debnam')).toMatchObject({
        buacMemberId: 1028,
        last: 'Debnam-Smith',
        gender: 'F',
      })
    })
  })
})

describe('standarising rows', () => {
  test('Need number for NEW_RUNNER in graphql', () => {
    expect(NewRunner).toBeLessThan(0)
  })
  test('MemberUnknown', () => {
    expect(MemberNumberUnknown).toBeLessThan(0)
  })
  test('Modern Handicap Row', async () => {
    const modernHandicapRow = {
      Place: '1',
      Member: '1005',
      First: 'Ann-Marie',
      Last: 'Wangdi',
      Start: '00:10',
      Pace: '07:59',
      'Est Time': '0:47:54.0',
    }
    expect(Object.keys(await normaliseCsv(modernHandicapRow))).toEqual(
      expect.arrayContaining([
        'place',
        'member',
        'estTime',
        'start',
        'first',
        'last',
        //'fullname',
      ])
    )
  })
  test('Modern Results Row', async () => {
    const row = {
      place: '1',
      first: 'Mel',
      last: 'Debnam',
      netTime: '0:38:50.4',
      pace: '06:28',
      diff: '-09:03.6',
      fullname: 'Mel Debnam',
    }
    expect(Object.keys(await normaliseCsv(row))).toEqual(
      expect.arrayContaining(['member', 'fullname'])
    )
  })
  test('Modern NEW results row', async () => {
    const row = await normaliseCsv({
      Place: 'NEW',
      First: 'Chase',
      Last: 'Carter',
      'NET Time': '0:07:27.0',
      Pace: '07:27',
    })
    expect(Object.keys(row)).toEqual(
      expect.arrayContaining(['netTime', 'pace', 'place'])
    )
    expect(row.place).toEqual(NewRunner)
  })
})
