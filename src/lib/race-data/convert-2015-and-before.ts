#!/usr/bin/env yarn ts-node

import { kebabCase, isNumber } from 'lodash'
import * as fs from 'fs'
import * as path from 'path'
import * as firstline from 'firstline'
import * as csv from 'csvtojson'
import {
  walk,
  transformFrontmatter,
  csvHeaderToFileType,
} from './on-race-folder.js'

const sourceTop = '../buac-jekyll/raceDataCSV'
const destTop = './src/race-data'
const years = [2009, 2010, 2011, 2012, 2013, 2014, 2015]

const dontCopy = [
  // messages from brad.
  // '2015/03_UniInvitation/handicaps/medium/03_inv_handicap_4km_15_auto__tb18_1.csv',
  // '2015/03_UniInvitation/handicaps/medium/03_inv_handicap_2km_15_auto__tb22_1.csv',
  // '2015/03_UniInvitation/handicaps/short/03_inv_handicap_short15_auto__tb19_1.csv',
  // '2015/03_UniInvitation/handicaps/long/03_Inv_handicap_sen15_auto__tb37_1.csv',
]
const removeFirstLines = [
  /MALE/i,
  /UNDER/,
  /OPEN /,
  /Handicap Points/i,
  /Sealed Handicap/i,
]

for (const year of years) {
  console.log('Looking for year', year)
  const filenames = walk(path.join(sourceTop, year.toString()))
  filenames
    .filter(fn => path.basename(fn) === 'frontMatter.yaml')
    .filter(fn => fn.match(/2009/))
    //.filter(fn => fn.match(/Colleg/))
    .forEach(fmf => {
      const fm = transformFrontmatter({ dir: path.dirname(fmf) })
      const newFmf = path.join(
        destTop,
        year.toString(),
        fmf.replace(new RegExp(`.*/${year}/`), '')
      )
      console.log(`fm ${newFmf}`)
      console.log(`race FrontMatter ${fmf} into ${newFmf} `)
      fs.mkdirSync(path.dirname(newFmf), { recursive: true })
      fs.copyFileSync(fmf, newFmf)

      walk(path.dirname(fmf))
        .filter(f => f.match(/\.csv$/))
        //.filter(fn => fn.match(/handicaps/))
        .forEach(csvFn => {
          if (dontCopy.some(ignore => csvFn.includes(ignore))) {
            console.log(`Ignore ${csvFn}`)
            return null
          }
          console.log(`csv ${csvFn}`)
          let { dir: newPath, base: newBase } = path.parse(
            csvFn.replace(new RegExp(`.*/${year}/`), '')
          )
          const lines = fs
            .readFileSync(csvFn)
            .toString()
            .split('\n')
          if (removeFirstLines.some(r => lines[0].match(r))) {
            console.log(` remove ${lines[0]} from ${csvFn}`)
            const removed = lines.shift()
            newBase = `${kebabCase(removed.split(',')[0])}.csv`
          }

          const newFn = path.join(destTop, year.toString(), newPath, newBase)
          console.log(` firstline ${lines[0]}`)
          console.log(` into ${newFn}`)
          csv({ checkType: true })
            .fromString(lines.join('\n'))
            .preFileLine((line, idx) =>
              // First Name is no good...
              idx === 0
                ? line
                    .toLowerCase()
                    .replace(/ /g, '')
                    .replace(/\./g, '')
                    .replace('firstname', 'first')
                    .replace('surname', 'last')
                    .replace('lastname', 'last')
                    .replace('name', 'first') //2011
                    .replace(',time', ',net Time')
                    // 2015.replace(',est,', ',est Time,')
                    // 2015.replace(/,est$/, ',est Time,')
                    // 2015.replace('est.time', 'est Time')
                    // pace in handicaps. Pace is pace....
                    .replace('estpace', 'Pace')
                    // 2014 one col mins, one secs
                    .replace('estmins/km', 'pace')
                    .replace('estmins/km', 'paceSecs')

                    .replace('estmin/km', 'pace')
                    .replace('estmin/km', 'paceSecs')

                    // 2011
                    .replace('esttime/km', 'pace')
                    .replace('esttime/km', 'paceSecs')
                    // 2015.replace('estpace', 'Pace')
                    // 2015.replace('est.pace', 'Pace')
                    .replace('nettime', 'net Time')
                    .replace('+or-', 'diff') // 15 college its one column
                    .replace('difference', 'amount') // 15 college its one column
                    .replace('amounttohandicap', 'amount') // 09 college its one column

                    .replace('plusorminus', 'diff')
                    .replace('esttime', 'est Time')
                    .replace('order', 'place')
                    .replace('tagno', 'member')
                    .replace(',tag,', ',member,')
                    .replace('min/km', 'pace')
                    .replace('starttime', 'start')
                    .replace('pm', 'diff')
              .replace(/,diff$/, ',amount')
              .replace('+/-','diff')
              .replace(',by',',amount')
                : line
            )
            .then(async rows => {
              if (rows.length > 0) {
                rows.forEach(row => {
                  ;['diff', 'first', 'last'].forEach(key => {
                    if (row.hasOwnProperty(key) && isNumber(row[key])) {
                      console.log(
                        `AAAA Numeric '${key}':'${row[key]}' in ${newFn}  ${csvFn}`
                      )
                    }
                  })
                  if (row.diff && isNumber(row.diff)) {
                    console.log(`AAAAA Numeric Diff ${row.diff} ${newFn}`, {
                      row,
                    })
                    //throw new Error(`Numeric Diff ${newFn}${row.diff}`)
                    //process.exit(99)
                  }
                  if (
                    row.hasOwnProperty('amount') &&
                    row.hasOwnProperty('diff')
                  ) {
                    //2014
                    row.diff = `${row.diff} ${row.amount}`
                    delete row.amount
                  }
                  //2014
                  if (row.pace && row.hasOwnProperty('paceSecs')) {
                    row.pace = `${row.pace}:${row.paceSecs}`
                    delete row.paceSecs
                  }
                  delete row.paidup
                  if (row.estTime) {
                    row.estTime.replace(/\./g, ':')
                  }
                })
                fs.mkdirSync(path.dirname(newFn), { recursive: true })
                const header = Object.keys(rows[0])
                const hRow = header.join(',')
                const content = [hRow]
                  .concat(rows.map(r => header.map(h => r[h]).join(',')))
                  .join('\n')
                fs.writeFileSync(newFn, content)
                const buacType = csvHeaderToFileType(hRow, newFn)
                if (!buacType) {
                  console.log(csvFn)
                }
              } else {
                console.log(`short file ${newFn.replace(destTop, '')}`)
              }
            })
        })
    })
}
