// Lets pass a folder to the data gathering and see what happens.  Saves
// running gatsby develop for errors/debug we can do with rerun

const { onRaceFolder } = require('./on-race-folder')
// const { memberByName } = require('./members')
const MembersDB = require('./members')

;(async function() {
  if (false) {
    // Test member load
    const me = await MembersDB.memberByName('Andrew Creer')
    const megan = await MembersDB.memberByName('Megan McDonald')
    console.log({ me, megan })
  }
  const samples = [
    //'src/race-data/2015/01_CollegeClassic/',
    //'src/race-data/2015/02_ApolloHillAttack/',
    ////'src/race-data/2015/03_UniInvitation',
    //'src/race-data/2015/06_WoodvaleClassic/',
    //'src/race-data/2015/05_SandhurstSlog/',
    //'src/race-data/2015/04_HamStreetHustle/',
    //'src/race-data/2015/15_CrusoeCrusade/',
    //'src/race-data/2015/18_HalfMarathonFestival/',
    //'src/race-data/2016/17_BuacHalfMarathon/',
    //'src/race-data/2019/01_college-classic',
    //'src/race-data/2019/03_uni-invite/',
    //'src/race-data/2019/14_mystery-run',
    'src/race-data/2019/15_half-marathon-festival/',
  ]
  for (const dir of samples) {
    const { csvs, meta } = await onRaceFolder({ dir })
    console.log({ meta })
    console.log(csvs[9])
  }
})()
