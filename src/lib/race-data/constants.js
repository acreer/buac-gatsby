exports.HANDICAP='HANDICAP' //Club handicap
exports.RESULT='RESULT' // Club Points Race
exports.POINTS='POINTS' // Club Points (speed or aggregate)

exports.POINTS_SPEED='POINTS_SPEED'
exports.POINTS_CLUB='POINTS_CLUB'


// Other Race. invite Half mara, melb mara?  Mystery run.  Try to have less csv
// types in general? Use the names of races to display Guess or something. Have
// an OTHER column maybe? 
exports.OTHER_RESULT='OTHER_RESULT'  

// Club race lengths
exports.SHORT='SHORT'  
exports.LONG='LONG'
exports.MEDIUM='MEDIUM'

// Need a number for graphql or all numbers are undefined
exports.MemberNumberUnknown = -1
exports.NewRunner = -2
