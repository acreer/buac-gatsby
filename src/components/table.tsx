import React from 'react'
import { useState } from 'react'
import styled from '@emotion/styled'
import { sortBy, startCase } from 'lodash'

const Styles = styled.div`
  padding: 1rem;
  table {
    border-spacing: 0;
    border: 1px solid black;
    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }
    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;
      :last-child {
        border-right: 0;
      }
    }
    th {
      background-color: yellow;
    }
  }
`

const initialSortDesc = false
export const Table = ({ columns, data }) => {
  const [sortCol, setSortCol] = useState('')
  const [sortDesc, setSortDesc] = useState(initialSortDesc)
  // No data, no table
  if(!data){return null}
  const sortedData = sortCol ? sortBy(data, sortCol) : data

  if (sortCol && sortDesc) {
    sortedData.reverse()
  }

  return (
    <Styles>
      <table>
        <thead>
          <tr>
            {columns.map((column, cx) => (
              // Add the sorting props to control sorting. For this example
              // we can add them into the header props
              <th
                key={cx}
                onClick={() => {
                  if (sortCol === column.accessor) {
                    setSortDesc(!sortDesc)
                  } else {
                    setSortDesc(initialSortDesc)
                    setSortCol(column.accessor)
                  }
                }}
              >
                {column.header ? column.header : startCase(column.accessor)}
                <span>
                  {sortCol === column.accessor
                    ? sortDesc
                      ? ' 🔽'
                      : ' 🔼'
                    : ''}
                </span>
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {sortedData.map((row, rx) => (
            <tr key={rx}>
              {columns.map((cell, cx) => {
                return (
                  <td key={cx}>
                    {cell.render
                      ? cell.render(row[cell.accessor], row)
                      : row[cell.accessor]}
                  </td>
                )
              })}
            </tr>
          ))}
        </tbody>
      </table>
    </Styles>
  )
}
