import { jsx, css } from '@emotion/core'
import styled from '@emotion/styled'
import { Link as GatsbyLink } from 'gatsby'
import * as React from 'react'
import Media from 'react-media'

// import shield from '../images/shield.jpg'
// console.log('shield',shield)
const shield = require('../images/shield.jpg')
// const shield = await import ('../images/shield.jpg')
// const shield='shield'

const linkStyle = css({
  color: 'yellow',
  padding: '0 1em',
  whiteSpace: 'nowrap',
})

const Link = styled(GatsbyLink)(linkStyle)
const A = styled.a(linkStyle)

const SiteTitle = () => (
  <div>
    <Media query={{ maxWidth: 499 }}>
      {matches =>
        matches ? (
          'BUAC'
        ) : (
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <img style={{ width: 40, height: 40 }} src={shield} alt="logo" />
            <p>Bendigo University Athletics Club</p>
          </div>
        )
      }
    </Media>
  </div>
)

const StyledHeader = styled.header({
  background: `black`,
  display: 'flex',
  justifyContent: 'space-between',
  padding: '0 0.75em',
})

const NavUl = styled.ul({
  alignItems: 'center',
  display: 'flex',
  justifyContent: 'space-between',
})
const NavLi = styled.li({ listStyleType: 'none' })

const Header = () => (
  <StyledHeader>
    <div>
      <Link to="/">
        <SiteTitle />
      </Link>
    </div>
    <NavUl>
      <NavLi>
        <Link to="/results">Results</Link>
      </NavLi>
      <NavLi>
        <Link to="/members">People</Link>
      </NavLi>
      <NavLi>
        <Link to="/news">News</Link>
      </NavLi>
    </NavUl>
  </StyledHeader>
)

export default Header
