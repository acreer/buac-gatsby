import React from 'react'
import { Link } from 'gatsby'

import { MemberNumberUnknown } from '../lib/race-data/constants'

export const MemberLink = ({ member, children }) =>
  member === MemberNumberUnknown ? (
    children
  ) : (
    <Link to={`/members/${member}`}>{children}</Link>
  )

export const memberRender = (val, row) => (
  <>
    <a id={row['member']}></a>
    <MemberLink member={row['member']}>{val}</MemberLink>
  </>
)
