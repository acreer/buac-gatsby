//import { jsx, css } from '@emotion/core'
//import styled from '@emotion/styled'
import { Link } from 'gatsby'
import * as React from 'react'
import { startCase } from 'lodash'

import { IfcRunNode } from '../interfaces'
interface Prop {
  nodes: any
  title: string
  slug?: string
}

export const niceLength = length =>
  length
    .toLowerCase()
    .split('.')
    .map(b => startCase(b))
    .join('.')
    .replace(' Km', ' km')
    .replace(/U (\d)/g, ' U$1')

const LengthLink = ({ id, slug, buacLength, buacTypeExtra, thisSlug, ...rest }) => (
  <Link key={id} to={slug}>
    <button disabled={slug === thisSlug}>
      {niceLength(buacLength)}
      {buacTypeExtra ? ` ${niceLength(buacTypeExtra)}` : null}
      {rest.csv ? ` (${rest.csv.length})` : null}
    </button>
  </Link>
)

export const CsvLengthLinks = (prop: Prop) => {
  const { nodes, title } = prop
  const byLength =
    nodes.length > 6
      ? nodes.reduce((acc, { node }) => {
          const key = node.buacLength
          if (!acc[key]) {
            acc[key] = []
          }
          //node.buacLength = ''
          acc[key].push(node)
          return acc
        }, {})
      : null
  if (nodes.length === 0) return null
  if (byLength) {
    const divs = Object.entries(byLength).map(
      ([buacLength, nodes]: [string, IfcRunNode[]]) => (
        <div key={buacLength}>
          <h4>{niceLength(buacLength)} </h4>
          <div>
            {nodes.map(node => LengthLink({ ...node, thisSlug: prop.slug }))}
          </div>
        </div>
      )
    )
    return <div>{divs}</div>
  }
  return (
    <div>
      <span>{title}:</span>
      {nodes.map(({ node }) => LengthLink({ ...node, thisSlug: prop.slug }))}
    </div>
  )
}
