import React from 'react'
import { graphql, Link } from 'gatsby'
import { startCase } from 'lodash'
import styled from '@emotion/styled'

import { LONG, MEDIUM, SHORT } from '../lib/race-data/constants'

export const lengthColor = length => {
  switch (length) {
    case LONG:
      return {
        backgroundColor: '#A93226',
        color: 'white',
      }
    case MEDIUM:
      return {
        backgroundColor: '#0B5345',
        color: 'white',
      }
    case SHORT:
      return {
        backgroundColor: '#D35400',
        color: 'white',
      }
    default:
      return {
        backgroundColor: '#626567',
        color: 'white',
      }
  }
}

const LengthButton = styled.button((props: { buacLength: string }) => ({
  ...lengthColor(props.buacLength),
  borderRadius: 6,
}))

export default LengthButton
