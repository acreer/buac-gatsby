/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

// import { useStaticQuery,graphql } from 'gatsby'
import { css, Global } from '@emotion/core'
import emotionNormalize from 'emotion-normalize'
import { StaticQuery } from 'gatsby'
import { graphql } from 'gatsby'
import * as React from 'react'

import Header from './header'

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Global
          styles={css`
            ${emotionNormalize}
            html, body {
              padding: 0;
              margin: 0;
              background: white;
              min-height: 100%;
              font-family: 'Droid Sans', Helvetica, Arial, sans-serif;
            }
          `}
        />
        <Header />
        <div style={{}}>
          <main>{children}</main>
          <footer>
            © {new Date().getFullYear()}, Built with
            {` `}
            <a href="https://www.gatsbyjs.org">Gatsby</a>
          </footer>
        </div>
      </>
    )}
  />
)

/*
Layout.propTypes = {
  children: PropTypes.node.isRequired,
};
 */
export default Layout
