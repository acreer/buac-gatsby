export interface IfcRunNode {
  id: string
  slug: string
  buacYear: string
  buacRunName: string
  buacLength: string
  buacTypeExtra: string
  buacRunNumber: number
  uniqueRunners: number
}
