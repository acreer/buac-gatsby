import { graphql } from 'gatsby'

export const BannerImageProcessFragment = graphql`
  fragment BannerImageProcessFragment on File {
    childImageSharp {
      fluid(maxWidth: 1200, maxHeight: 400) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`

export const buacTypeResultFrag = graphql`
  fragment buacTypeResultFrag on buacTypeResultConnection {
    edges {
      node {
        id
        slug
        buacLength
        csv {
          member
          first
          last
        }
      }
    }
  }
`
export const raceOccursRaceYear = graphql`
  fragment raceOccursRaceYear on Query {
    raceOccurs(buacYear: { eq: $buacYear }, buacRunId: { eq: $buacRunId }) {
      slug
      uniqueRunners
      id
    }
  }
`

export const buacFilesRaceYear = graphql`
  fragment buacFilesRaceYear on Query {
    allBuacTypeResult(
      filter: { buacYear: { eq: $buacYear }, buacRunId: { eq: $buacRunId } }
    ) {
      ...buacTypeResultFrag
      totalCount
    }

    allBuacTypeHandicap(
      filter: { buacYear: { eq: $buacYear }, buacRunId: { eq: $buacRunId } }
    ) {
      edges {
        node {
          id
          slug
          buacLength
        }
      }
      totalCount
    }

    allBuacTypePoints(
      filter: { buacYear: { eq: $buacYear }, buacRunId: { eq: $buacRunId } }
    ) {
      edges {
        node {
          id
          slug
          buacLength
          buacTypeExtra
        }
      }
      totalCount
    }

    allBuacTypeOtherResult(
      filter: { buacYear: { eq: $buacYear }, buacRunId: { eq: $buacRunId } }
    ) {
      edges {
        node {
          id
          slug
          buacLength
          buacTypeExtra
          csv {
            member
            first
            last
          }
        }
      }
      totalCount
    }
  }
`
