import React from 'react'
import { graphql, Link } from 'gatsby'
import { startCase } from 'lodash'
import styled from '@emotion/styled'

import Layout from '../components/layout'
import LengthButton, { lengthColor } from '../components/length-button'
import { IfcRunNode } from '../interfaces'

import { LONG, MEDIUM, SHORT } from '../lib/race-data/constants'

const RaceLink = ({
  buacMemberId,
  node: {
    id,
    slug,
    buacYear,
    buacType,
    buacTypeExtra,
    buacRunNumber,
    buacRunName,
    buacRunId,
    buacRunDate,
    buacLength,
  },
}) => (
  <Link key={id} to={`${slug}#${buacMemberId}`}>
    <button>{`${buacYear} ${buacRunName} ${startCase(
      buacLength.toLowerCase()
    )} ${startCase(buacTypeExtra)}`}</button>
  </Link>
)

import { maleColor, femaleColor } from '../style/constants'

const startLower = str => (str ? startCase(str.toLowerCase()) : '')

const GenderHeading = styled.h2((props: { gender: string }) => ({
  color: props.gender === 'M' ? maleColor : femaleColor,
}))

export default ({ data, pageContext }) => {
  const { id, name, buacMemberId, gender } = pageContext
  const {
    allBuacTypeResult: { edges: clubResults },
    allBuacTypeOtherResult: { edges: otherResults },
  } = data

  const byYear = clubResults.concat(otherResults).reduce((acc, { node }) => {
    const key = node.buacYear
    if (!acc[key]) {
      acc[key] = []
    }
    acc[key].push(node)
    return acc
  }, {})
  return (
    <Layout>
      <GenderHeading gender={gender}>{name}</GenderHeading>
      {Object.entries(byYear)
        .sort((a, b) => b[0].localeCompare(a[0]))
        .map(([year, nodes]: [string, IfcRunNode[]]) => (
          <div key={year}>
            <h4>{year}</h4>
            <div style={{ margin: 3 }}>
              {[LONG, MEDIUM, SHORT, 'Other'].map(l => (
                <span key={l} style={{ ...lengthColor(l), padding: 5 }}>
                  {l}
                </span>
              ))}
            </div>
            {nodes.map(
              ({ id, slug, buacLength, buacRunName, buacTypeExtra }) => (
                <Link key={slug} to={`${slug}#${buacMemberId}`}>
                  <LengthButton buacLength={buacLength}>
                    {startLower(buacRunName)} {startLower(buacLength)}{' '}
                    {startLower(buacTypeExtra)}
                  </LengthButton>
                </Link>
              )
            )}
          </div>
        ))}
    </Layout>
  )
}

export const pageQuery = graphql`
  query($buacMemberId: Int!) {
    allBuacTypeResult(
      filter: { csv: { elemMatch: { member: { eq: $buacMemberId } } } }
    ) {
      edges {
        node {
          buacYear
          buacType
          buacRunNumber
          buacRunName
          buacRunId
          buacRunDate
          buacLength
          slug
        }
      }
    }

    allBuacTypeOtherResult(
      filter: { csv: { elemMatch: { member: { eq: $buacMemberId } } } }
    ) {
      edges {
        node {
          buacLength
          buacRunDate
          buacRunId
          buacRunName
          buacRunNumber
          buacType
          buacYear
          filename
          slug
          buacTypeExtra
        }
      }
    }
  }
`
