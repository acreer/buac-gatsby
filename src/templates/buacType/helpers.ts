export const definedKeys = ob =>
  Object.keys(ob).reduce((acc, k) => {
    acc[k] = ob[k]
    return acc
  }, {})
