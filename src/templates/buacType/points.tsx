import React from 'react'
import { useState } from 'react'
import { graphql, Link } from 'gatsby'
import { startCase } from 'lodash'

import Layout from '../../components/layout'
import { Table } from '../../components/table'
import { NewRunner } from '../../lib/race-data/constants'
import { MemberLink, memberRender } from '../../components/member-link'
import { CsvLengthLinks } from '../../components/csv-slug-links'

const ResultsTable = ({ csv }) => {
  const columns = [
    { accessor: 'place' },
    { accessor: 'first', render: memberRender },
    { accessor: 'last', render: memberRender },
    { accessor: 'points' },
    { accessor: 'races' },
  ]
  return <Table columns={columns} data={csv} />
}

export default ({ pageContext, data }) => {
  const {
    buacRunDate,
    buacRunId,
    buacRunName,
    buacRunNumber,
    buacYear,
    buacLength,
    csv,
    slug,
  } = pageContext
  const { edges: results } = data.allBuacTypeResult
  const { edges: handicaps } = data.allBuacTypeHandicap
  const { edges: points } = data.allBuacTypePoints
  const { edges: otherResults } = data.allBuacTypeOtherResult
  const { raceOccurs } = data
  return (
    <Layout>
      <h1>
        {startCase(buacLength.toLowerCase())} Points {buacRunName} -{' '}
        <Link to={raceOccurs.slug}>
          Race {buacRunNumber} in {buacYear}
        </Link>
      </h1>
      <CsvLengthLinks nodes={points} title="Points" slug={slug} />
      <ResultsTable csv={csv} />
      <CsvLengthLinks nodes={results} title="Results" />
      <CsvLengthLinks nodes={handicaps} title="Handicaps" />
      <CsvLengthLinks nodes={otherResults} title="Results" />
    </Layout>
  )
}

// TODO this graphql is same as in race-handicap.  Refactor
export const query = graphql`
  query($buacYear: Int, $buacRunId: String!) {
    ...raceOccursRaceYear
    ...buacFilesRaceYear
  }
`
