import React from 'react'
import { useState } from 'react'
import { graphql, Link } from 'gatsby'
import { startCase } from 'lodash'

import Layout from '../../components/layout'
import { Table } from '../../components/table'
import { NewRunner } from '../../lib/race-data/constants'
import { MemberLink, memberRender } from '../../components/member-link'
import { CsvLengthLinks } from '../../components/csv-slug-links'
import { definedKeys } from './helpers'

const HandicapTable = ({ csv }) => {
  const theseKeys = definedKeys(csv[0])
  const columns = [
    { accessor: 'pace' },
    { accessor: 'first', render: memberRender },
    { accessor: 'last', render: memberRender },
    { accessor: 'start' },
    { accessor: 'estTime' },
    {
      accessor: 'place',
      render: val => (val === NewRunner ? 'NEW' : val),
    },
  ].filter(c => theseKeys[c.accessor])
  return <Table columns={columns} data={csv} />
}

export default ({ pageContext, data }) => {
  const {
    buacRunDate,
    buacRunId,
    buacRunName,
    buacRunNumber,
    buacYear,
    buacLength,
    csv,
    slug,
  } = pageContext
  const { edges: results } = data.allBuacTypeResult
  const { edges: handicaps } = data.allBuacTypeHandicap
  const { edges: points } = data.allBuacTypePoints
  const {
    raceOccurs: { slug: raceOccursSlug },
  } = data
  return (
    <Layout>
      <h1>
        {startCase(buacLength.toLowerCase())} Handicaps {buacRunName} -{' '}
        <Link to={raceOccursSlug}>
          Race {buacRunNumber} in {buacYear}
        </Link>
      </h1>
      <CsvLengthLinks title="Results" nodes={results} />
      <CsvLengthLinks title="Handicaps" nodes={handicaps} slug={slug} />
      <CsvLengthLinks title="Points" nodes={points} />
      <HandicapTable csv={csv} />
    </Layout>
  )
}

// TODO this graphql is same as in race-result.  Refactor
export const query = graphql`
  query($buacYear: Int, $buacRunId: String!) {
    ...buacFilesRaceYear
    ...raceOccursRaceYear
  }
`
