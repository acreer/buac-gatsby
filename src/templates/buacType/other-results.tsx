import React from 'react'
import { useState } from 'react'
import { graphql, Link } from 'gatsby'
import styled from '@emotion/styled'
import { startCase } from 'lodash'

import Layout from '../../components/layout'
import { Table } from '../../components/table'
import { NewRunner } from '../../lib/race-data/constants'
import { MemberLink, memberRender } from '../../components/member-link'
import { CsvLengthLinks, niceLength } from '../../components/csv-slug-links'
import { definedKeys } from './helpers'

const ResultsTable = ({ csv }) => {
  const theseKeys = definedKeys(csv[0])
  const columns = [
    { accessor: 'first', render: memberRender },
    { accessor: 'last', render: memberRender },
    { accessor: 'time' },
    { accessor: 'netTime' },
    { accessor: 'pace' },
    {
      accessor: 'place',
      render: val => (val === NewRunner ? 'NEW' : val),
    },
  ].filter(c => theseKeys[c.accessor])
  return <Table columns={columns} data={csv} />
}

const Row = styled.div({ display: 'flex' })

export default ({ pageContext, data }) => {
  const {
    buacRunDate,
    buacRunId,
    buacRunName,
    buacRunNumber,
    buacYear,
    buacLength,
    csv,
    slug,
  } = pageContext
  const { edges: results } = data.allBuacTypeResult
  const { edges: otherResults } = data.allBuacTypeOtherResult
  const { edges: handicaps } = data.allBuacTypeHandicap
  const { edges: points } = data.allBuacTypePoints
  const { raceOccurs } = data
  return (
    <Layout>
      <h1>
        {niceLength(buacLength)} Results {buacRunName} -{' '}
        <Link to={raceOccurs.slug}>
          Race {buacRunNumber} ({ raceOccurs.uniqueRunners }) in {buacYear}
        </Link>
      </h1>
      <Row>
        <div>
          <CsvLengthLinks nodes={results} title="Results" />
          <CsvLengthLinks nodes={otherResults} title="Results" slug={slug} />
          <CsvLengthLinks nodes={handicaps} title="Handicaps" />
          <CsvLengthLinks nodes={points} title="Points" />
        </div>
        <div>
          <ResultsTable csv={csv} />
        </div>
      </Row>
    </Layout>
  )
}

// TODO this graphql is same as in race-handicap.  Refactor
export const query = graphql`
  query($buacYear: Int, $buacRunId: String!) {
    ...raceOccursRaceYear
    ...buacFilesRaceYear
  }
`
