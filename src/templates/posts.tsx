import React from 'react'
import { graphql, Link } from 'gatsby'
import { startCase } from 'lodash'
import styled from '@emotion/styled'

import Layout from '../components/layout'

const PrevNext = ({ previousPagePath, nextPagePath }) => (
  <>
    <Link to="/news">
      <button disabled={previousPagePath === ''}>Latest</button>
    </Link>
    <Link to={previousPagePath}>
      <button disabled={previousPagePath === ''}>Previous</button>
    </Link>
    <Link to={nextPagePath}>
      <button disabled={nextPagePath === ''}>Next</button>
    </Link>
  </>
)

export default ({ data, pageContext }) => {
  const { edges: posts } = data.allWpPost
  const {
    humanPageNumber,
    nextPagePath,
    previousPagePath,
    numberOfPages,
  } = pageContext
  return (
    <Layout>
      <h1>
        Page {humanPageNumber} of {numberOfPages}
      </h1>
      <PrevNext
        nextPagePath={nextPagePath}
        previousPagePath={previousPagePath}
      />
      <div>
        {posts.map(
          ({
            node: {
              id,
              title,
              excerpt,
              content,
              slug,
              path,
              jetpack_featured_media_url,
            },
          }) => {
            console.log({ excerpt, content })
            return (
              <div key={id}>
                <div
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <div>
                    <Link to={`/news/${slug}`}>
                      <h2 dangerouslySetInnerHTML={{ __html: title }} />
                    </Link>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: excerpt ? excerpt : content,
                      }}
                    />
                  </div>
                  <div style={{ width: '50%' }}>
                    {jetpack_featured_media_url ? (
                      <img
                        style={{
                          width: '100%',
                          height: 'auto',
                        }}
                        src={jetpack_featured_media_url}
                      />
                    ) : null}
                  </div>
                </div>
              </div>
            )
          }
        )}
      </div>
      <PrevNext
        nextPagePath={nextPagePath}
        previousPagePath={previousPagePath}
      />
    </Layout>
  )
}

export const postQuery = graphql`
  query($limit: Int!, $skip: Int!) {
    site {
      siteMetadata {
        title
      }
    }

    allWpPost(
      sort: { order: DESC, fields: modified_gmt }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
          id
          author {
            name
          }
          jetpack_featured_media_url
          slug
          title
          excerpt
          content
        }
      }
    }
  }
`
