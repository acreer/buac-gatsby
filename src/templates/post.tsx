import React from 'react'
import { graphql, Link } from 'gatsby'
import { startCase } from 'lodash'
import styled from '@emotion/styled'

import Layout from '../components/layout'

export default ({ pageContext }) => {
  const { title, content } = pageContext
  //console.log(post)
  //console.log(pageContext)
  return (
    <Layout>
      <h1 dangerouslySetInnerHTML={{ __html: title }} />
      <div dangerouslySetInnerHTML={{ __html: content }} />
    </Layout>
  )
}

