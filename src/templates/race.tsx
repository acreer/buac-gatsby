import React from 'react'
import { useState } from 'react'
import { graphql, Link, navigate } from 'gatsby'

import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  LabelList,
} from 'recharts'

import Layout from '../components/layout'
import { CsvLengthLinks, niceLength } from '../components/csv-slug-links'

export default ({ data, pageContext }) => {
  const {
    buacRunDate,
    buacRunId,
    buacRunName,
    buacRunNumber,
    buacYear,
    uniqueRunners,
  } = pageContext
  const { edges: results } = data.allBuacTypeResult
  const { edges: handicaps } = data.allBuacTypeHandicap
  const { edges: points } = data.allBuacTypePoints
  const { edges: otherResults } = data.allBuacTypeOtherResult
  const { edges: otherYears } = data.allRaceOccurs

  const lengthGraphData = results.reduce(
    (acc, { node: { csv, buacLength, slug } }) => {
      //acc[niceLength(buacLength)] = csv.length
      acc.push({ name: niceLength(buacLength), value: csv.length, slug })
      return acc
    },
    []
  )
  const otherYearData = otherYears.map(
    ({ node: { slug, buacYear, uniqueRunners } }) => ({
      slug,
      buacYear,
      uniqueRunners,
    })
  )

  return (
    <Layout>
      <h1>
        {buacRunName} Race #{buacRunNumber} in {buacYear} with {uniqueRunners}{' '}
        runners
      </h1>
      {lengthGraphData.length > 0 ? (
        <ResponsiveContainer height={300}>
          <BarChart data={lengthGraphData}>
            <XAxis type="category" dataKey="name" />
            <YAxis type="number" />
            <Tooltip />
            <Bar
              dataKey="value"
              fill="crimson"
              onClick={({ slug }) => navigate(slug)}
            />
          </BarChart>
        </ResponsiveContainer>
      ) : null}

      <CsvLengthLinks title="Results" nodes={results} />
      <CsvLengthLinks title="Results" nodes={otherResults} />
      <CsvLengthLinks title="Handicaps" nodes={handicaps} />
      <CsvLengthLinks title="Points" nodes={points} />

      <ResponsiveContainer height={300}>
        <BarChart data={otherYearData}>
          <Bar
            dataKey="uniqueRunners"
            fill="green"
            onClick={({ slug }) => navigate(slug)}
          />
          <Tooltip />
          <XAxis dataKey="buacYear" />
          <YAxis />
        </BarChart>
      </ResponsiveContainer>

      {otherYears.map(
        ({ node: { id, slug, buacYear: oBuacYear, uniqueRunners } }) => (
          <Link key={id} to={slug}>
            <button disabled={oBuacYear == buacYear}>
              {oBuacYear} ({uniqueRunners})
            </button>
          </Link>
        )
      )}
    </Layout>
  )
}

export const query = graphql`
  query($buacYear: Int, $buacRunId: String!) {
    ...buacFilesRaceYear
    allRaceOccurs(
      filter: { buacRunId: { eq: $buacRunId }, buacYear: {} }
      sort: { fields: buacYear, order: DESC }
    ) {
      edges {
        node {
          id
          slug
          buacYear
          buacRunNumber
          uniqueRunners
        }
      }
      totalCount
    }
  }
`
