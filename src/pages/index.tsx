import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'

const IndexPage = () => (
  <Layout>
    <SEO title="Buac Home Page" />
    <p>Hi</p>
  </Layout>
)

export default IndexPage
