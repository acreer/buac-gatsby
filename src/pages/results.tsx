import { Link, graphql, navigate } from 'gatsby'
import * as React from 'react'
import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
  LabelList,
} from 'recharts'

import Layout from '../components/layout'
import SEO from '../components/seo'

import { IfcRunNode } from '../interfaces'

const uniqueRunnersData = nodes =>
  nodes
    .sort((a, b) => a.buacRunNumber - b.buacRunNumber)
    .map(({ buacRunName, uniqueRunners, slug }) => ({
      buacRunName: buacRunName.replace(/\(.*\)/, '').replace(/-.*/, ''),
      uniqueRunners,
      slug,
    }))

const RunsPage = ({ data }) => {
  const { edges: runs } = data.allRaceOccurs
  const byYear = runs.reduce((acc, { node }) => {
    const key = node.buacYear
    if (!acc[key]) {
      acc[key] = []
    }
    acc[key].push(node)
    return acc
  }, {})
  const graphData = Object.keys(byYear).reduce((acc, year) => {
    acc[year] = uniqueRunnersData(byYear[year])
    return acc
  }, {})
  return (
    <Layout>
      <SEO title="Results" />
      <h1>Results</h1>
      {Object.entries(byYear)
        .sort((a, b) => b[0].localeCompare(a[0]))
        .map(([year, nodes]: [string, IfcRunNode[]]) => (
          <div key={year}>
            <h2>{year}</h2>
            {graphData[year].some(e => e.uniqueRunners > 0) ? (
              <ResponsiveContainer height={300}>
                <BarChart data={graphData[year]} margin={{ bottom: 50 }}>
                  <XAxis dataKey="buacRunName" tick={false} />
                  <YAxis />
                  <Tooltip />
                  <Bar
                    onClick={({ slug }) => navigate(slug)}
                    dataKey="uniqueRunners"
                    fill="#8884d8"
                  >
                    <LabelList dataKey="buacRunName" position="bottom" />
                    <LabelList dataKey="uniqueRunners" position="center" />
                  </Bar>
                </BarChart>
              </ResponsiveContainer>
            ) : null}

            {nodes
              .sort((a, b) => a.buacRunNumber - b.buacRunNumber)
              .map(({ id, slug, buacRunName, uniqueRunners }) =>
                uniqueRunners > 0 ? (
                  <Link key={id} to={slug}>
                    <button>
                      {buacRunName} ({uniqueRunners})
                    </button>
                  </Link>
                ) : null
              )}
          </div>
        ))}
    </Layout>
  )
}

export default RunsPage

export const query = graphql`
  {
    allRaceOccurs(sort: { order: DESC, fields: buacRunDate }) {
      edges {
        node {
          buacRunDate
          buacRunId
          buacRunNumber
          buacRunName
          buacYear
          slug
          id
          uniqueRunners
        }
      }
    }
  }
`
