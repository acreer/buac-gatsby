import React from 'react'
import { useState } from 'react'
import { Link, graphql } from 'gatsby'
import styled from '@emotion/styled'
import { ceil } from 'lodash'

import Layout from '../components/layout'
import SEO from '../components/seo'
import {maleColor,femaleColor} from '../style/constants'

const GenderButton = styled.button((props: { gender: string }) => ({
  color: props.gender === 'M' ? maleColor : femaleColor,
  backgroundColor: props.gender === 'M' ? 'orange' : 'yellow',
  borderRadius:6
}))

const MemberButton = ({ member }) => (
  <Link to={member.slug}>
    <GenderButton gender={member.gender}>
      ({member.buacMemberId}) {member.first} {member.last}
    </GenderButton>
  </Link>
)

const Members = ({ members }) =>
  members.length ? (
    members.map(edge => <MemberButton key={edge.node.id} member={edge.node} />)
  ) : (
    <p>None</p>
  )

const PaginatedMembers = ({ members }) => {
  const [pageLength, setPageLength] = useState(50)
  const [page, setPage] = useState(1)

  if (members.length < pageLength) {
    return <Members members={members} />
  }
  const currentPage = members.slice((page - 1) * pageLength, page * pageLength)
  const pages = ceil(members.length / pageLength)
  return (
    <>
      <div>
        <button disabled={page < 2} onClick={() => setPage(page - 1)}>
          Prev
        </button>
        Page {page} of {pages}
        <button disabled={page > pages - 1} onClick={() => setPage(page + 1)}>
          Next
        </button>
      </div>
      <Members members={currentPage} />
    </>
  )
}

const MemberPage = ({
  data: {
    allBuacMember: { totalCount, edges },
  },
}) => {
  const [filter, setFilter] = useState('')
  const reg = new RegExp(`${filter}`, 'i')
  const members = edges.filter(
    ({ node: {  name, buacMemberId } }) =>
      name.match(reg) || `${buacMemberId}` === filter
  )
  return (
    <Layout>
      <SEO title="All the members" />
      <p>
        {members.length} of {totalCount} members Name matching '{filter}'
      </p>
      <input
        type="text"
        value={filter}
        placeholder="Name or Number"
        onChange={e => setFilter(e.target.value)}
      />
      {filter !== '' ? <button onClick={() => setFilter('')}>X</button> : null}
      <div>
        <PaginatedMembers members={members} />
      </div>
    </Layout>
  )
}

export const query = graphql`
  query {
    allBuacMember(
      sort: { order: ASC, fields: buacMemberId }
      filter: { gender: { in: ["F", "M"] } }
    ) {
      totalCount
      edges {
        node {
          id
          buacMemberId
          buacTag
          dob
          first
          last
          name
          gender
          slug
        }
      }
    }
  }
`

export default MemberPage
