/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// We are in node land. Libs come in a bit old school.
const moment = require('moment')
const csv = require('csvtojson')
const path = require('path')
const { camelCase, isDate } = require('lodash')
const { paginate } = require('gatsby-awesome-pagination')

const { onRaceFolder } = require('./src/lib/race-data/on-race-folder')
const { createFilePath } = require('gatsby-source-filesystem')
const MembersDB = require('./src/lib/race-data/members.js')

const raceSlugBase = 'race'
const fs = require('fs')

// These are csv files.  The csvInfo is merged frontMatter and the file
// contents, WITH member added

const csvNode = ({ csvInfo, createContentDigest, dir }) => {
  //TODO type checking that all the bits are here
  const {
    buacType,
    buacRunDate,
    buacRunId,
    buacLength,
    buacTypeExtra,
    filename,
  } = csvInfo
  const date = moment(buacRunDate).format('YYYY-MM-DD')
  const slug = ['', raceSlugBase, date, buacRunId, buacType, buacLength]
  if (buacTypeExtra) {
    slug.push(buacTypeExtra)
  }
  return {
    id: filename,
    ...csvInfo,
    slug: slug
      .join('/')
      .toLowerCase()
      .replace(/ /g, '-'),
    parent: null,
    children: [],
    internal: {
      type: camelCase(`buacType${buacType}`),
      contentDigest: createContentDigest(csvInfo),
    },
  }
}

// All front matters even if no files.
const occursNode = ({ occurs, createContentDigest }) => {
  //TODO check for standard inputs.
  const buacRunDate = moment(occurs.buacRunDate).format('YYYY')
  const { buacRunId } = occurs
  return {
    id: `race-occurs-${buacRunDate}-${buacRunId}`,
    parent: null,
    children: [],
    ...occurs,
    slug: ['', raceSlugBase, buacRunDate, buacRunId].join('/').toLowerCase(),
    internal: {
      type: 'RaceOccurs',
      contentDigest: createContentDigest(occurs),
    },
  }
}

// DATA 3. Make graphQL nodes.
//
// This could be a plugin, to scan a tree of race-data looking for special
// files.
//
//   * The members-list is ALL the club members.
//
//   * Each frontMatter.yaml is a race event, and marks a tree of results
//   hadicaps, points etc
//
// SLUGS put the slug in the node, IF you are going to create a page Then we
// can always find the link to the page in a graphql query
exports.onCreateNode = async (
  { node, actions, loadNodeContent, createContentDigest },
  options
) => {
  const { createNode, createParentChildLink } = actions
  if (node.sourceInstanceName === 'wpPosts' && node.internal.type === 'File') {
    const posts = JSON.parse(fs.readFileSync(node.absolutePath))
    posts.forEach(
      ({
        id,
        date_gmt,
        modified_gmt,
        slug,
        jetpack_featured_media_url,
        title: { rendered: title },
        content: { rendered: content },
        excerpt: { rendered: excerpt },
        _embedded: { author },
      }) => {
        const data = {
          id: `wpPost${id}`,
          slug,
          date_gmt,
          modified_gmt,
          title,
          jetpack_featured_media_url,
          content,
          excerpt,
          author: author[0],
        }
        createNode({
          parent: null,
          children: [],
          internal: {
            type: 'WpPost',
            contentDigest: createContentDigest(content),
          },
          ...data,
        })
      }
    )
  }
  // Only nodes found by the raceData in gatsby-config.js
  if (node.sourceInstanceName === 'raceData') {
    // frontMatter.yaml means we have a race on our hands.  So we proces the
    // subtree and gets a whole lot of data
    // console.log('node.base',node.base)
    if (node.base === 'frontMatter.yaml') {
      // console.log(`doing ${node.dir}/frontMatter`)
      const { csvs, meta } = await onRaceFolder({ dir: node.dir })
      const parent = createNode(
        occursNode({ occurs: meta, createContentDigest })
      )
      for (const csvInfo of csvs) {
        const child = createNode(
          csvNode({ csvInfo, createContentDigest, dir: node.dir })
        )
      }
      //console.log(`done ${node.dir}/frontMatter`)
    }

    // The memberlist  name also in lib/race-data/members.js
    if (node.base === 'member.csv') {
      const members = MembersDB.members
      //console.log({ members })
      for (const data of members) {
        if (!isDate(data.dob)) {
          //console.log(`Bad date for ${data.name} in member.csv`)
        } else {
          // console.log(`member node ${data.name}`)
          data.slug = `/members/${data.buacMemberId}`
          try {
            createNode({
              id: `buac-member-${data.buacMemberId}`,
              parent: null,
              ...data,
              children: [],
              internal: {
                type: 'BuacMember',
                contentDigest: createContentDigest(data),
              },
            })
          } catch (e) {
            console.log(e)
            console.log({ data })
          }
        }
      }
    }
  }
}

// Data 4, use the NODES to make pages for each item.
//
// The pattern is query the NODES, and then map each one to page with its own
// template
//
// General Pattern define the node with graphQl and pump the node into the page
// as the pageContext

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  // wpPotst
  const posts = await graphql(`
    {
      allWpPost {
        edges {
          node {
            id
            slug
            content
            excerpt
            title
            author {
              name
            }
          }
        }
      }
    }
  `)
  paginate({
    createPage,
    items: posts.data.allWpPost.edges,
    itemsPerPage: 10,
    pathPrefix: '/news',
    component: path.resolve(`./src/templates/posts.tsx`),
  })
  posts.data.allWpPost.edges.forEach(({ node }) =>
    createPage({
      path: `/news/${node.slug}`,
      component: path.resolve(`./src/templates/post.tsx`),
      context: { ...node },
    })
  )

  // Members
  const members = await graphql(`
    {
      allBuacMember {
        edges {
          node {
            buacMemberId
            dob
            first
            name
            gender
            id
            last
            buacTag
            slug
          }
        }
      }
    }
  `)
  /*
  members.data.allBuacMember.edges.forEach(({ node }) =>
    createPage({
      path: node.slug,
      component: path.resolve(`./src/templates/member.tsx`),
      context: { ...node },
    })
  )
  */
  for (const { node } of members.data.allBuacMember.edges) {
    // console.log('allBuacMember page')
    createPage({
      path: node.slug,
      component: path.resolve(`./src/templates/member.tsx`),
      context: { ...node },
    })
  }

  // Race Occurs or is scheduled.  Ie a calendar
  const races = await graphql(`
    {
      allRaceOccurs {
        edges {
          node {
            buacRunDate
            buacRunId
            buacRunName
            buacRunNumber
            buacYear
            id
            slug
            uniqueRunners
          }
        }
      }
    }
  `)
  for (const { node } of races.data.allRaceOccurs.edges) {
    // console.log(`allRaceOccurs page ${node.slug}`)
    createPage({
      path: node.slug,
      component: path.resolve(`./src/templates/race.tsx`),
      context: {
        ...node,
      },
    })
  }

  // Results for points
  const clubResults = await graphql(`
    {
      allBuacTypeResult {
        edges {
          node {
            buacLength
            buacRunDate
            buacRunId
            buacRunName
            buacRunNumber
            buacType
            buacYear
            id
            slug
            filename
            csv {
              first
              last
              name
              member
              pace
              netTime
              diff
              place
            }
          }
        }
      }
    }
  `)
  for (const { node } of clubResults.data.allBuacTypeResult.edges) {
    const { slug, filename } = node
    createPage({
      path: slug,
      component: path.resolve('./src/templates/buacType/results.tsx'),
      context: {
        ...node,
      },
    })
  }

  // Handicaps
  const handicaps = await graphql(`
    {
      allBuacTypeHandicap {
        edges {
          node {
            buacLength
            buacRunId
            buacRunDate
            buacRunName
            buacRunNumber
            buacType
            buacYear
            id
            slug
            filename
            csv {
              estTime
              first
              name
              last
              member
              pace
              place
              start
            }
          }
        }
      }
    }
  `)
  for (const { node } of handicaps.data.allBuacTypeHandicap.edges) {
    // console.log('allBuacTypeHandicap page')
    createPage({
      path: node.slug,
      component: path.resolve('./src/templates/buacType/handicaps.tsx'),
      context: {
        ...node,
      },
    })
  }
  const pointsCsvs = await graphql(`
    {
      allBuacTypePoints {
        edges {
          node {
            buacLength
            buacRunDate
            buacRunId
            buacRunName
            buacRunNumber
            buacType
            buacYear
            slug
            filename
            csv {
              first
              name
              last
              member
              place
              points
              races
            }
          }
        }
      }
    }
  `)

  for (const { node } of pointsCsvs.data.allBuacTypePoints.edges) {
    // console.log('allBuacTypePoints Page')
    createPage({
      path: node.slug,
      component: path.resolve('./src/templates/buacType/points.tsx'),
      context: node,
    })
  }
  const otherResults = await graphql(`
    {
      allBuacTypeOtherResult {
        edges {
          node {
            buacLength
            buacRunDate
            buacRunId
            buacRunName
            buacRunNumber
            buacType
            buacYear
            filename
            slug
            csv {
              club
              name
              first
              last
              member
              pace
              place
              time
              netTime
              age
              diff
              gender
              guess
            }
          }
        }
      }
    }
  `)
  for (const { node } of otherResults.data.allBuacTypeOtherResult.edges) {
    // console.log('otherResults Page')
    createPage({
      path: node.slug,
      context: node,
      component: path.resolve('./src/templates/buacType/other-results.tsx'),
    })
  }
}
