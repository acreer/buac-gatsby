module.exports = {
  pathPrefix: `/data`,
  siteMetadata: {
    title: `Bendigo Univerity Athletics Club`,
    description: `We enjoy running with the pack. 
We are not all fast and we don't all finish first, but we are all runners and will always be part of the pack.`,
    author: `@gatsbyjs`,
  },
  plugins: [
    `gatsby-plugin-emotion`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-typescript-checker`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-mdx`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        // DATA 2. 
        name: `wpPosts`,
        path: `${__dirname}/wp-cache/post`,
      },
    },
    {
      // DATA 1. Here we look in race-data for files.
      resolve: `gatsby-source-filesystem`,
      options: {
        // DATA 2. gatsby-node needs to see raceData files.
        name: `raceData`,
        path: `${__dirname}/src/race-data`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/data`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/shield.jpg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['Droid Sans', 'Droid Serif'],
        },
      },
    },
  ],
}
